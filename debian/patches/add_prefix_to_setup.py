Index: pass-import/Makefile
===================================================================
--- pass-import.orig/Makefile	2019-12-19 22:43:32.915176082 +0100
+++ pass-import/Makefile	2019-12-19 22:44:46.687868699 +0100
@@ -24,7 +24,7 @@
 	@install -v -m 0644 "pass-$(PROG).1" "$(DESTDIR)$(MANDIR)/man1/pass-$(PROG).1"
 	@install -v -m 0644 "completion/pass-$(PROG).bash" "$(DESTDIR)$(BASHCOMPDIR)/pass-$(PROG)"
 	@install -v -m 0644 "completion/pass-$(PROG).zsh" "$(DESTDIR)$(ZSHCOMPDIR)/_pass-$(PROG)"
-	@python3 setup.py install --root="$(DESTDIR)" --optimize=1 --skip-build
+	@python3 setup.py install --root="$(DESTDIR)" --optimize=1 --skip-build --prefix="$(PREFIX)"
 	@echo
 	@echo "pass-$(PROG) is installed succesfully"
 	@echo
@@ -42,7 +42,7 @@
 local:
 	@install -v -d "$(DESTDIR)$(PASSWORD_STORE_EXTENSIONS_DIR)/"
 	@install -v -m 0755 "$(PROG).bash" "$(DESTDIR)$(PASSWORD_STORE_EXTENSIONS_DIR)/$(PROG).bash"
-	@python3 setup.py install --user --prefix= --optimize=1
+	@python3 setup.py install --user --prefix= --optimize=1 --prefix="$(PREFIX)"
 	@echo
 	@echo "pass-$(PROG) is localy installed succesfully."
 	@echo "Remember to set to 'true' PASSWORD_STORE_ENABLE_EXTENSIONS for the extension to be enabled."
